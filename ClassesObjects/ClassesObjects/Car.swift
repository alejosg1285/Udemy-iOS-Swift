//
//  Car.swift
//  ClassesObjects
//
//  Created by Alejo Saa G on 31/10/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import Foundation

enum CarType {
    case Sedan
    case Coupe
    case HatchBack
}

class Car {
    var colour = "Black"
    var numberOfSeats : Int = 5
    var typeOfCar : CarType = .Coupe
    
    // Designated initizilizer
    init() {
        
    }
    
    // Convenience initializer
    convenience init(customerColour : String) {
        self.init()
        colour = customerColour
    }
    
    func drive() {
        print("car roll out")
    }
}
