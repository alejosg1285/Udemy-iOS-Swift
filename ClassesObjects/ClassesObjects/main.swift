//
//  main.swift
//  ClassesObjects
//
//  Created by Alejo Saa G on 29/10/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import Foundation

print("Hello, World!")

let myFord = Car()

print(myFord.colour)
print(myFord.numberOfSeats)
print(myFord.typeOfCar)

myFord.colour = "Red"
print(myFord.colour)

let myBmw = Car(customerColour: "White")

print(myBmw.colour)
print(myBmw.numberOfSeats)
print(myBmw.typeOfCar)

myFord.drive()

let autoCar = SelfDrivingCar()
autoCar.destination = "1 hacker way"
autoCar.drive()
print(autoCar.colour)
