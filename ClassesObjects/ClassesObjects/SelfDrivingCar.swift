//
//  SelfDrivingCar.swift
//  ClassesObjects
//
//  Created by Alejo Saa G on 3/11/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import Foundation

class SelfDrivingCar : Car {
    var destination : String?
    
    override func drive() {
        super.drive()
        
        // Optional binding
        if let userSetDestination = destination {
            print("Drive towards " + userSetDestination)
        }
    }
}
