//: Playground - noun: a place where people can play

import UIKit

// variable
var age : Int = 33

// constante
let firstName : String = "Alejandro"

// interpolación de variables en cadenas.
let fullName = "\(firstName) Saa G"

let detailsMe = "\(fullName), \(age)"

func calcBMI(peso : Double, altura: Double) -> String {
    var strResult : String = ""
    let bmi : Double =  peso / pow(altura, 2)
    let shortenedBMI = String(format: "%.2f", bmi)
    
    if bmi > 25 {
        strResult = "estas pasado de kilos"
    } else if bmi > 18.5 {
        strResult = "estas bien de peso"
    } else {
        strResult = "hace falta tomarse la sopita"
    }
    
    return "Tu indice de masa corporal es \(shortenedBMI) y el resultado es: \(strResult)"
}

print(calcBMI(peso: 78, altura: 1.77))


func cancionCervezas(frias: Int) -> String {
    var letra : String = ""
    
    for number in (1...frias).reversed() {
        let estrofa : String = "\n\(number) botellas de frias en la hielera, \(number) botellas en la hielera. \nSaca una y pasala, \(number - 1) frias en la hielera.\n"
        
        letra += estrofa
    }
    
    letra += "\nSe acabaron las frias, no hay frias en la hielera. \nVe a la tienda y compra mas, \(frias) frias en la hielera.\n"
    
    return letra
}

print(cancionCervezas(frias: 10))

func fibonacci(hasta : Int) {
    var num1 : Int = 0
    var num2 : Int = 1
    
    print(num1)
    
    // Si no se va a utilizar la variable creada en el ciclo, se reemplaza por un guion bajo
    for _ in (0...hasta) {
        let num : Int = num1 + num2
        print(num)
        
        num1 = num2
        num2 = num
    }
}

fibonacci(hasta: 5)

// using clousures
func calculator(n1: Int, n2: Int, operation: (Int, Int) -> Int) -> Int {
    return operation(n1, n2)
}

func add(n1: Int, n2: Int) -> Int {
    return n1 + n2
}

func multiply(n1: Int, n2: Int) -> Int {
    return n1 * n2
}

calculator(n1: 3, n2: 5, operation: add)

let result = calculator(n1: 3, n2: 3) {$0 * $1}
print(result)
