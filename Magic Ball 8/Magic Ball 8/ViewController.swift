//
//  ViewController.swift
//  Magic Ball 8
//
//  Created by Alejo Saa G on 25/02/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var imageBall: UIImageView!
    
    let arrBall = ["ball1", "ball2", "ball3", "ball4", "ball5"]
    
    var randomBall = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        askQuestion()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func askQuestionPress(_ sender: UIButton) {
        askQuestion()
    }
    
    func askQuestion() {
        randomBall = Int(arc4random_uniform(5))
        
        imageBall.image = UIImage(named: arrBall[randomBall])
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        askQuestion()
    }
}

