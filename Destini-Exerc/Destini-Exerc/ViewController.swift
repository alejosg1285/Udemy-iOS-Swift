//
//  ViewController.swift
//  Destini-Exerc
//
//  Created by Alejo Saa G on 15/07/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var lbHistory: UILabel!
    @IBOutlet weak var btnAnswer1: UIButton!
    @IBOutlet weak var btnAnswer2: UIButton!
    @IBOutlet weak var btnRestart: UIButton!
    // Texts
    let story1 = "Your car has blown a tire on a winding road in the middle of nowhere with no cell phone reception. You decide to hitchhike. A rusty pickup truck rumbles to a stop next to you. A man with a wide brimmed hat and soulless eyes opens the passenger door for you and says: \"Need a ride, boy?\""
    let story2 = "He nods slowly, unphased by the question."
    let story3 = "As you begin to drive, the stranger starts talking about his relationship with his mother. He gets angrier and angrier by the minute. He asks you to open the glovebox. Inside you find a bloody knife, two severed fingers, and a cassette tape of Elton John. He reaches for the glove box."
    let story4 = "What? Such a cop out! Did you know traffic accidents are the second leading cause of accidental death for most adult age groups?"
    let story5 = "As you smash through the guardrail and careen towards the jagged rocks below you reflect on the dubious wisdom of stabbing someone while they are driving a car you are in."
    let story6 = "You bond with the murderer while crooning verses of \"Can you feel the love tonight\". He drops you off at the next town. Before you go he asks you if you know any good places to dump bodies. You reply: \"Try the pier.\""
    
    let answer1a = "I\'ll hop in. Thanks for the help!"
    let answer1b = "Better ask him if he\'s a murderer first."
    let answer2a = "At least he\'s honest. I\'ll climb in."
    let answer2b = "Wait, I know how to change a tire."
    let answer3a = "I love Elton John! Hand him the cassette tape."
    let answer3b = "It\'s him or me! You take the knife and stab him."
    
    var pathStory:Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        restartGame()
        
        btnRestart.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func chooseHistory(_ sender : AnyObject) {
        if sender.tag == 1 && pathStory == 1 || sender.tag == 1 && pathStory == 2 {
            lbHistory.text = story3
            btnAnswer1.setTitle(answer3a, for: .normal)
            btnAnswer2.setTitle(answer3b, for: .normal)
            pathStory = 3
        } else if sender.tag == 2 && pathStory == 1 {
            lbHistory.text = story2
            btnAnswer1.setTitle(answer2a, for: .normal)
            btnAnswer2.setTitle(answer2b, for: .normal)
            pathStory = 2
        } else if sender.tag == 2 && pathStory == 2 {
            lbHistory.text = story4
            btnAnswer1.isHidden = true
            btnAnswer2.isHidden = true
            pathStory = 4
        } else if sender.tag == 1 && pathStory == 3 {
            lbHistory.text = story6
            btnAnswer1.isHidden = true
            btnAnswer2.isHidden = true
            pathStory = 6
        } else if sender.tag == 2 && pathStory == 3 {
            lbHistory.text = story5
            btnAnswer1.isHidden = true
            btnAnswer2.isHidden = true
            pathStory = 5
        }
        
        if pathStory == 4 || pathStory == 5 || pathStory == 6 {
            btnRestart.isHidden = false
        }
    }
    
    @IBAction func pressRestart(_ sender: UIButton) {
        restartGame()
    }
    
    func restartGame() {
        lbHistory.text = story1
        btnAnswer1.setTitle(answer1a, for: .normal)
        btnAnswer2.setTitle(answer1b, for: .normal)
        btnAnswer1.isHidden = false
        btnAnswer2.isHidden = false
    }
}

