//
//  Question.swift
//  Pregunton
//
//  Created by Alejo Saa G on 7/07/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import Foundation

class Question {
    let questionText : String
    let answer : Bool
    
    init(text: String, correctAnswer: Bool) {
        questionText = text
        answer = correctAnswer
    }
}
