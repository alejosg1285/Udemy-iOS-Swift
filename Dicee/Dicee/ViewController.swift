//
//  ViewController.swift
//  Dicee
//
//  Created by Alejo Saa G on 24/02/18.
//  Copyright © 2018 Alejo Saa G. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var randomDiceIndex1 : Int = 0
    var randomDiceIndex2 : Int = 0

    @IBOutlet weak var diceImageView1: UIImageView!
    @IBOutlet weak var diceImageView2: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        changeDiceNumber()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func rollButtonPressed(_ sender: UIButton) {
        changeDiceNumber()
    }
    
    func getNumberRandom() -> Int {
        return (Int(arc4random_uniform(6))) + 1
    }
    
    func changeDiceNumber() {
        randomDiceIndex1 = getNumberRandom()
        randomDiceIndex2 = getNumberRandom()
        
        diceImageView1.image = UIImage(named: "dice\(randomDiceIndex1)")
        diceImageView2.image = UIImage(named: "dice\(randomDiceIndex2)")
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        changeDiceNumber()
    }
    
}

